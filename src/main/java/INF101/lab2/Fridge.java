package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {

    ArrayList<FridgeItem> ItemList = new ArrayList<>();
    int maxCapacity = 20;

    @Override
    public void emptyFridge() {
        ItemList.clear();
        
    }

    @Override
    public int nItemsInFridge() {
        return ItemList.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(ItemList.size()<20){
            ItemList.add(item);
            return true;
        }
        return false;
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> ExpiredItems = new ArrayList<>();
        for(FridgeItem item : ItemList){
            if(item.hasExpired()){
                ExpiredItems.add(item);
                
            }
        }
        ItemList.removeAll(ExpiredItems);
        return ExpiredItems;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(ItemList.contains(item)){
            ItemList.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public int totalSize() {
        
        return maxCapacity;
    }
    
}
